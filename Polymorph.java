package WhatPolyLikes;

/**
 *
 * @author denis
 */
public class Polymorph {
   public static void main(String[] args) {
      SteelHandsWings shw = new SteelHandsWings("JetWings", "Aviation", false, 200, 30);
      
      SmartPhone smsng = new SmartPhone("Samsung S6", true, 5, "QC820", "A7 Nougat");
      Ipad ipd = new Ipad("Ipad mini 4", true, 8, "IOS 10");
      
      System.out.println(shw.Perfomance());
      System.out.println(shw.toString());
      System.out.println(shw.IsUseful());
      
      System.out.println(smsng.Perfomance());
      System.out.println(smsng.toString());
      System.out.println(smsng.MusicPlay());
      System.out.println(smsng.wifishare()); //creating wifi hotspot
      System.out.println(smsng.IsUseful());
      
      System.out.println(ipd.Perfomance());
      System.out.println(ipd.toString());
      System.out.println(ipd.MusicPlay());
      System.out.println(ipd.IsUseful());
      
      //upcast
      Mobiles dvc = new SmartPhone("Iphone 7", true, 5, "A9","IOS 10");
      System.out.println(dvc.Perfomance());
      System.out.println(dvc.toString());
      System.out.println(dvc.MusicPlay());
      //System.out.println(dvc.wifishare()); //can't create wifi hotspot because of upcast
      System.out.println(dvc.IsUseful());
    }
}
