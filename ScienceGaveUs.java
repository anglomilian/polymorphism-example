package WhatPolyLikes;
/**
 *
 * @author denis
 */
public abstract class ScienceGaveUs {
  String name;
  String division;
  boolean useful;
  
  //constructor
  public void ScinceGaveUS(String name, String division, boolean useful) { 
    this.name = name;
    this.division = division;
    this.useful = useful;
  }
  
  public String IsUseful() {
    if(this.useful) return "Realy useful.";
    else return "It's just for fun!";
  }
  
  //Creating common interface for different devices
  public abstract String Perfomance();
  
  @Override
  public String toString(){
   return name + ", division: " + division;   
  }
 
}
